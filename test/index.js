const assert = require('assert');
const {sayHello} = require('../src');

describe('sayHello', () => {
  const testCases = [
    {
      passed: 'World',
      expected: 'Hello, World!'
    },
    {
      passed: 'EPAM',
      expected: 'Hello, EPAM!'
    },
    {
      passed: 'EHU',
      expected: 'Hello, EHU!'
    }
  ]

  context('when not implemented', function () {
    it.only('should throw an Error unless present', function () {
    });
  });

  context('when implemented', function () {
    testCases.forEach((testCase) => {
      it(`should return "${testCase.expected}"`, () => {
        const message = testCase.passed ? sayHello(testCase.passed) : sayHello();
        assert.equal(message, testCase.expected);
      });
    })
  });
});
